= Antora {extension} Pipeline Extension
:pipeline-version: 2.3.0
:extension-version: 1.0.0-alpha.4-0.0.3
:extension: pdf
:source-repository: https://gitlab.com/djencks/antora-{extension}
:description: This is an adaptation of asciidoctor-web-pdf to an Antora pipeline extension.

WARNING: The pipeline extension system is not an official part of Antora.
See https://gitlab.com/antora/antora/-/issues/377.
If you contemplate using this project (and the associated @djencks/pdf-ui project) you should expect several incompatible updates before it becomes stable.

== Description

{description}

The code is located at link:{source-repository}[]

This project includes the modifications to Antora needed to generate html suitable for conversion to a paged pdf using `paged.js`, and the steps needed to send it to Chromium to render into pdf, and to add the output to the Antora content catalog so it will be published as part of the site.
In addition, you will need the UI modifications from the pdf-ui project to provide the javascript (paged.js) and css for Chromium to render pdfs properly.
The pdf-ui project provides a prebuilt integration of these additions with the Antora default UI.
Consult the pdf-ui project for more information on customizing this.


== Installation

Use a package.json file to specify and locally install the dependencies needed for your documentation project.
This example assumes no other extensions or customizations:

[source,json,subs="+attributes"]
----
{
  "description": "---",
  "scripts": {
    "simple-install": "npm i --cache=.cache/npm --no-optional",
    "build": "antora --generator @djencks/site-generator-default antora-playbook.yml --stacktrace --fetch",
    "clean-build": "npm run clean-install;npm run build",
    "clean-install": "rm -rf node_modules/ .cache/ package-lock.json ;npm i --cache=.cache/npm"
  },
  "devDependencies": {
    "@antora/cli": "^2.3.3",
    "@djencks/content-aggregator": "https://experimental-repo.s3-us-west-1.amazonaws.com/djencks-content-aggregator-v{pipeline-version}.tgz",
    "@djencks/site-generator-default": "https://experimental-repo.s3-us-west-1.amazonaws.com/djencks-site-generator-default-v{pipeline-version}.tgz",
    "@djencks/playbook-builder": "https://experimental-repo.s3-us-west-1.amazonaws.com/djencks-playbook-builder-v{pipeline-version}.tgz",
    "@djencks/antora-{extension}": "https://experimental-repo.s3-us-west-1.amazonaws.com/djencks-antora-{extension}-v{extension-version}.tgz",
    "@djencks/pdf-ui": "https://experimental-repo.s3-us-west-1.amazonaws.com/djencks-pdf-ui-v{extension-version}.tgz"
  }
}
----

NOTE: For normal use, the `simple-install` and `build` scripts are adequate.
If you are developing antora-pdf and attempting to reinstall the same version of any packages, the `clean-install` script will defeat npm caching and allow you to run the updated code.

Include this in your `antora-playbook.yml` playbook, replacing the `ui` key:

[source,yml,subs="+attributes"]
----
ui:
  bundle:
    url: ./node_modules/@djencks/pdf-ui/build/pdf-ui-bundle.zip <1>
extensions:
  - path: "@djencks/antora-{extension}"
    config:
      pdfFiles: <2>
        - images:plantuml-embedded.adoc
        - ROOT:highlight.adoc
      suppressHtml: false <3>
      suspendServer: true <4>
----
<1> To successfully render pdfs appropriately, the UI bundle needs to include the javascript (primarily paged.js) and css needed by Chromium to render paged output.
The `@djencks/pdf-ui` project provides this combined with the Antora default UI.
Consult `@djencks/pdf-ui` for information about customizing this UI bundle.
<2> A list of files to convert to pdf.
Each entry is a filter in Antora resource ID format.
Each segment if specified must be an exact match, except for relative, which may be a picomatch expression.
Files do not need to be ordinarily publishable; typically using a hidden file for pdf is appropriate.
<3> Normally both html (for all pages) and pdf (for configured pages) output is generated.
Set `suppressHtml: true` to generate only pdfs.
<4> Set `suspendServer: true` to process the html to pdf in Chromium, but not write the pdf to the file and suspend the internal server.
The urls (ending in `pdf`) are written to the command line, and you may debug paged.js in Chrome (the only browser supported by paged.js).
Eventually the html will be written to the attachments folder in place of the pdf, but without the css and js it can't be rendered.

Note that all pdf files are written to the attachments folder of the module they are from.
They may be linked to from other documents in the same module.

Run `npm run simple-install` and `npm run build`.
If you change this plugin, you need to run `npm run clean-install` to work around npm caching.

== Inclusions and building a pdf document out of one or more nav files

I strongly recommend avoiding the preprocessor `include::` instruction for including otherwise standalone pages into a 'master' pdf document, and instead using the `ainclude` block macro processor from `@djencks/asciidoctor-ainclude`.
This asciidoctor extension is installed automatically by `@djencks/antora-pdf`.
In contradistinction to the `include` preprocessor instruction, `ainclude` is semantically appropriate.
It will only include content that forms a valid (sub) document, and provides attribute isolation between included documents and their context.
In addition, in contradistinction to the `asciidoctor-pdf` behavior, it provide an id for the top of an included document, so that xrefs to a document can be transformed to just work rather than needing to be to a section id at the top of the document.

With an Antora site, the organization is normally expressed in one or more nav files.
These can be used directly to form a pdf, using the `listToAinclude` block macro from `@djencks/asciidoctor-ainclude`.
Generally you'll want a page to set the doc title, TOC parameters, and doctype.
Using the `listToAinclude` block macro on one or more nav files can be used to generate a pdf closely matching the site.

For instance, this produces a reasonable reference manual for the uyuni-docs project:

[source,adoc]
----
= Reference Guide: {productname} {productnumber}
:doctitle: Reference Guide: {productname} {productnumber}
:toc: auto
:toclevels: 4
:doctype: book
:sectnums:
:sectnumlevels: 5

listToAinclude::nav$nav-reference-guide.adoc[]
----

WARNING:: Due to some unfortunate assumptions in asciidoctor about embedded documents, you must explicitly specify the table separator in each table, like this:

[source,adoc]
----
[cols="1,1,1", options="header",separator=|]
|===
| Command | Description | Example Use
----

== Warnings

* Paged.js has problems with page breaks and occasionally enters infinite loops or cannot render tables.
* Much of the original code is still here for reference.
All that is used is under `packages`.
