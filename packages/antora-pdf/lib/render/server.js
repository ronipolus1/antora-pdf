const http = require('http')

function server (catalogs) {
  const site = catalogs.reduce((accum, catalog) => {
    catalog
      .getAll()
      .filter((file) => file.out)
      .reduce((accum2, file) => {
        if (!file.mediaType) {
          if (file.out.path.endsWith('.js')) {
            file.mediaType = 'application/javascript'
          } else if (file.out.path.endsWith('.css')) {
            file.mediaType = 'text/css'
          }
        }
        // console.log('file.out.path', file.out.path)
        accum2[file.out.path] = file
        return accum2
      }, accum)
    return accum
  }, {})
  const s = new http.Server((req, resp) => {
    const url = req.url.slice(1)
    const file = site[url]
    if (file) {
      console.log('request file', url)
      const type = file.mediaType
      // console.log('type', type)
      // if (type === 'text/html') {
      //   console.log('starting with ', file.contents.toString().slice(0, 200))
      // }
      type && resp.setHeader('Content-Type', type)
      resp.writeHead(200)
      resp.write(file.contents)
      resp.end()
    } else {
      console.log('file not found', url)
      resp.writeHead(404, 'file not found')
      resp.end()
    }
  }).listen({ port: 0, hostname: 'localhost' }) // Specifiying 0 for port will assign a random port
  return s
}

module.exports = server
