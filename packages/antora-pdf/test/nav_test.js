/* eslint-env mocha */
'use strict'

const { describe } = require('mocha')
const { expect } = require('chai')

const { loadAsciiDoc } = require('@djencks/asciidoc-loader')

const ainclude = require('@djencks/asciidoctor-ainclude')

const pdfTemplate = require('../lib/extensions/templates')
const aincludeIncludeProcessor = require('../lib/extensions/include/include-processor-wrapper')
const bodyAttributesProcessor = require('../lib/extensions/body-attributes-processor')

const mockContentCatalog = require('./antora-mock-content-catalog')

function setupAsciidoc (aincludeOptions) {
  const asciidocConfig = loadAsciiDoc.resolveConfig()

  // { attributes: {}, extensions: [], converters: [] }
  // asciidocConfig.attributes = {}//Object.assign({}, ac.attributes)
  asciidocConfig.extensions = asciidocConfig.extensions ? asciidocConfig.extensions : []
  asciidocConfig.converters = asciidocConfig.converters ? asciidocConfig.converters : []

  asciidocConfig.extensions.push(ainclude)
  asciidocConfig.extensions.push(aincludeIncludeProcessor)
  asciidocConfig.aincludeExtensions = [aincludeIncludeProcessor]
  asciidocConfig.extensions.push(bodyAttributesProcessor)
  asciidocConfig.converters.push(pdfTemplate)

  asciidocConfig.attributes['site-url'] = 'https://foo.com'
  asciidocConfig.attributes['ainclude-default-options'] = aincludeOptions
  return asciidocConfig
}

describe('nav to aincludes tests', () => {
  var contentCatalog

  beforeEach(() => {
    contentCatalog = mockContentCatalog([{
      version: '4.5',
      family: 'page',
      relative: 'page-a.adoc',
      contents: `This is a paragraph containing three sentences.
This is the second sentence.
This is the final sentence.
`,
    },
    {
      version: '4.5',
      family: 'page',
      relative: 'page-section.adoc',
      contents: `= This is section 1

With a paragraph with two sentences.
The second sentence.

== This is section 2

With a paragraph with two sentences.
The second sentence.

xref to section 1, this page xref:#_this_is_section_1[].
xref to section 2, this page xref:#_this_is_section_2[].
`,
    },
    {
      version: '4.5',
      family: 'page',
      relative: 'page-section2.adoc',
      contents: `= This is section 1

With a paragraph with two sentences, with duplication.
The second sentence.

== This is section 2

With a paragraph with two sentences, with duplication.
The second sentence.

xref to section 1, this page xref:#_this_is_section_1[].
xref to section 2, this page xref:#_this_is_section_2[].
`,
    },
    {
      version: '4.5',
      family: 'nav',
      relative: 'nav1.adoc',
      contents: `* xref:page-section.adoc[]
* xref:page-section2.adoc[]
`,
    },
    {
      version: '4.5',
      family: 'nav',
      relative: 'nav2.adoc',
      contents: `include::nav$nav1.adoc[]
`,
    },
    {
      version: '4.5',
      family: 'page',
      relative: 'doc1.adoc',
      contents: `= This is doc 1

listToAinclude::nav$nav1.adoc[]
`,
    },
    {
      version: '4.5',
      family: 'page',
      relative: 'doc2.adoc',
      contents: `= This is doc 2

listToAinclude::nav$nav2.adoc[]
`,
    },
    {
      version: '4',
      component: 'component-b',
      family: 'page',
      relative: 'parent1.adoc',
      contents: `= This is the parent1 document

== This is a parent section

The first sentence references the included doc via xref:4.5@component-a:module-a:page-section.adoc[].
The second sentence references the second section of the included doc via xref:4.5@component-a:module-a:page-section.adoc#_this_is_section_2[].

ainclude::4.5@component-a:module-a:page-section.adoc[+1]
`,
    },
    {
      version: '4',
      component: 'component-b',
      family: 'page',
      relative: 'parent-double-ids.adoc',
      contents: `= This is parent-double-ids.adoc

== This is a parent section

The first sentence references the included doc via xref:4.5@component-a:module-a:page-section.adoc[].
The second sentence references the second section of the included doc via xref:4.5@component-a:module-a:page-section.adoc#_this_is_section_2[].

The third sentence references the included doc via xref:4.5@component-a:module-a:page-section2.adoc[].
The fourth sentence references the second section of the included doc via xref:4.5@component-a:module-a:page-section2.adoc#_this_is_section_2[].

ainclude::4.5@component-a:module-a:page-section.adoc[+1]

ainclude::4.5@component-a:module-a:page-section2.adoc[+1]
`,
    },
    {
      version: '4',
      component: 'component-b',
      family: 'page',
      relative: 'parent-double-ids-nav.adoc',
      contents: `= This is parent-double-ids.adoc

== This is a parent section

The first sentence references the included doc via xref:4.5@component-a:module-a:page-section.adoc[].
The second sentence references the second section of the included doc via xref:4.5@component-a:module-a:page-section.adoc#_this_is_section_2[].

The third sentence references the included doc via xref:4.5@component-a:module-a:page-section2.adoc[].
The fourth sentence references the second section of the included doc via xref:4.5@component-a:module-a:page-section2.adoc#_this_is_section_2[].

listToAinclude::4.5@component-a:module-a:nav$nav-for-parent-double-ids.adoc[]
`,
    },
    {
      version: '4.5',
      component: 'component-a',
      family: 'nav',
      relative: 'nav-for-parent-double-ids.adoc',
      contents: `* xref:page-section.adoc[]

* xref:page-section2.adoc[]
`,
    },
    {
      version: '4',
      component: 'component-b',
      family: 'page',
      relative: 'parent-nested.adoc',
      contents: `== This is a parent section

The first sentence references the included doc via xref:4.5@component-a:module-a:page-section.adoc[].
The second sentence references the second section of the included doc via xref:4.5@component-a:module-a:page-section.adoc#_this_is_section_2[].

The third sentence references the included doc via xref:4.5@component-a:module-a:page-section-parent.adoc[].
The fourth sentence references the second section of the included doc via xref:4.5@component-a:module-a:page-section-parent.adoc#_this_is_section_4[].

ainclude::4.5@component-a:module-a:page-section-parent.adoc[+1]

`,
    },
    //ref to including top level doc
    {
      version: '4.5',
      family: 'page',
      relative: 'page-ref-to-parent.adoc',
      contents: `== This is section 1

The first sentence references the included doc via xref:page-ref-to-parent-child.adoc[].
The second sentence references the second section of the included doc via xref:page-ref-to-parent-child#_this_is_section_4[].

== This is section 2

With a paragraph with two sentences.
The second sentence.

ainclude::page-ref-to-parent-child.adoc[]
`,
    },
    {
      version: '4.5',
      family: 'page',
      relative: 'page-ref-to-parent-child.adoc',
      contents: `== This is section 3

The first sentence references the parent doc via xref:page-ref-to-parent.adoc#_this_is_section_1[].
The second sentence references the second section of the parent doc via xref:page-ref-to-parent#_this_is_section_2[].

== This is section 4

With a paragraph with two sentences.
The second sentence.
`,
    },
    //ref to page not in pdf
    {
      version: '4.5',
      family: 'page',
      relative: 'page-ref-non-pdf.adoc',
      contents: `== This is section 1

The first sentence references the parent doc via xref:page-ref-to-parent.adoc#_this_is_section_1[].
The second sentence references the second section of the parent doc via xref:page-ref-to-parent#_this_is_section_2[].

== This is section 2

With a paragraph with two sentences.
The second sentence.

ainclude::page-ref-to-parent-child.adoc[]
`,
    },
    //links to explicit ids
    {
      version: '4.5',
      family: 'page',
      relative: 'page-custom-ids.adoc',
      contents: `== This is section 3

[[sentence-one,first sentence]]
With a paragraph with two sentences, with duplication.
[[sentence-two,second sentence]]
The second sentence.

== This is section 4

[#sentence-three,reftext='third sentence']
With a paragraph with two sentences, with duplication.
[#sentence-four,reftext='fourth sentence']
The second sentence.

xref to sentence 1, this page xref:#sentence-one[].
xref to sentence 2, this page xref:#sentence-two[].
xref to sentence 3, this page xref:#sentence-three[].
xref to sentence 4, this page xref:#sentence-four[].
`,
    },
    {
      version: '4.5',
      family: 'page',
      relative: 'page-custom-ids-parent.adoc',
      contents: `== This is section 1

The first sentence references the included doc via xref:page-custom-ids.adoc[].
The second sentence references the second section of the included doc via xref:page-custom-ids.adoc#_this_is_section_4[].

== This is section 2

xref to sentence 1, this page xref:page-custom-ids.adoc#sentence-one[].
xref to sentence 2, this page xref:page-custom-ids.adoc#sentence-two[].
xref to sentence 3, this page xref:page-custom-ids.adoc#sentence-three[].
xref to sentence 4, this page xref:page-custom-ids.adoc#sentence-four[].

ainclude::page-custom-ids.adoc[]
`,
    },
    {
      version: '1.0',
      family: 'page',
      relative: 'simple/parent.adoc',
      contents: `= Ainclude parent page

This is a simple example of \`ainclude\` functionality

ainclude::simple/first-child.adoc[+1]

ainclude::simple/second-child.adoc[+1]
`,
    },
    {
      version: '1.0',
      family: 'page',
      relative: 'simple/first-child.adoc',
      contents: `= This is a first child page for the simple ainclude example

Here is some content, linking to the first section of the second page: xref:simple/second-child.adoc[].

== This is the second section of the first child page

Here is some content, linking to the second section of the second child page: xref:simple/second-child.adoc#_this_is_the_second_section_of_the_second_child_page[].

Here is a link to the custom content on the second page: xref:simple/second-child.adoc#_custom_id_2[].

Here is a link to the custom content on this page: xref:#_custom_id[]

'''
[[_custom_id,Custom Id]]
Here is a paragraph with a custom Id.
`,
    },
    {
      version: '1.0',
      family: 'page',
      relative: 'simple/second-child.adoc',
      contents: `= This is a second child page for the simple ainclude example

Here is some content, linking to the first section of the first page: xref:simple/first-child.adoc[].

== This is the second section of the second child page

Here is some content, linking to the second section of the first child page: xref:simple/first-child.adoc#_this_is_the_second_section_of_the_first_child_page[].

Here is a link to the custom content on the first page: xref:simple/first-child.adoc#_custom_id[].

Here is a link to the custom content on this page: xref:#_custom_id_2[]

'''
[[_custom_id_2,Custom Id]]
Here is a paragraph with a custom Id.
`,
    },
    {
      version: '4.5',
      family: 'page',
      relative: 'doc3-plainest.adoc',
      contents: `= This is doc 1

include::page-section-including.adoc[leveloffset=+1]
`,
    },
    {
      version: '4.5',
      family: 'page',
      relative: 'doc3-plain.adoc',
      contents: `= This is doc 1

ainclude::page-section-including.adoc[+1]
`,
    },
    {
      version: '4.5',
      family: 'page',
      relative: 'doc3-list.adoc',
      contents: `= This is doc 1

listToAinclude::nav$nav3.adoc[]
`,
    },
    {
      version: '4.5',
      family: 'nav',
      relative: 'nav3.adoc',
      contents: `* xref:page-section-including.adoc[]
`,
    },
    {
      version: '4.5',
      family: 'page',
      relative: 'page-section-including.adoc',
      contents: `= This is including section 1

With a paragraph with two sentences.
The second sentence.

include::page-section-included.adoc[leveloffset=+1]
`,
    },
    {
      version: '4.5',
      family: 'page',
      relative: 'page-section-included.adoc',
      contents: `= This is included section 1

With a paragraph with two sentences, with duplication.
The second sentence.

`,
    },
    ])
  })

  ;[
    {
      setup: () => setupAsciidoc('original_ids'),
      getIdprefix: (idprefix) => ['_'],
      getIdsuffix: (idsuffix) => idsuffix,
      name: 'original_ids',
    },
    {
      setup: () => setupAsciidoc('unique_ids'),
      getIdprefix: (idprefix) => [idprefix],
      getIdsuffix: (idsuffix) => '',
      name: 'unique_ids',
    },
  ].forEach(({ setup, getIdprefix, getIdsuffix, name }) => {
    const asciidocConfig = setup()

    it(`plain listToAinclude ${name}`, () => {
      const file = contentCatalog.getById({ component: 'component-a', version: '4.5', module: 'module-a', family: 'page', relative: 'doc1.adoc' })
      const idprefix = getIdprefix('_4_5_component_a_module_a_page_page_section_adoc_')
      const idprefix2 = getIdprefix('_4_5_component_a_module_a_page_page_section2_adoc_')
      const doc = loadAsciiDoc(file, contentCatalog, asciidocConfig)
      expect(doc.blocks.length).to.equal(2)
      const html = doc.convert()
      expect(html).to.equal(`<div class="title-document">
  <h1>This is doc 1</h1>
  
</div>
<div style="display: none;"><ul class="sectlevel1">
<li class="toc-entry"><a href="#${idprefix}this_is_section_1">This is section 1</a></li>
<li class="toc-sublist">
<ul class="sectlevel2">
<li class="toc-entry"><a href="#${idprefix}this_is_section_2">This is section 2</a></li>
</ul>
</li>
<li class="toc-entry"><a href="#${idprefix2}this_is_section_1_2">This is section 1</a></li>
<li class="toc-sublist">
<ul class="sectlevel2">
<li class="toc-entry"><a href="#${idprefix2}this_is_section_2_2">This is section 2</a></li>
</ul>
</li>
</ul></div>

<div id="content" class="content">
  <div class="sect1">
<h2 id="${idprefix}this_is_section_1"><a class="anchor" href="#${idprefix}this_is_section_1"></a>This is section 1</h2>
<div class="sectionbody">
<div class="paragraph">
<p>With a paragraph with two sentences.
The second sentence.</p>
</div>
<div class="sect2">
<h3 id="${idprefix}this_is_section_2"><a class="anchor" href="#${idprefix}this_is_section_2"></a>This is section 2</h3>
<div class="paragraph">
<p>With a paragraph with two sentences.
The second sentence.</p>
</div>
<div class="paragraph">
<p>xref to section 1, this page <a href="#${idprefix}this_is_section_1">This is section 1</a>.
xref to section 2, this page <a href="#${idprefix}this_is_section_2">This is section 2</a>.</p>
</div>
</div>
</div>
</div>
<div class="sect1">
<h2 id="${idprefix2}this_is_section_1_2"><a class="anchor" href="#${idprefix2}this_is_section_1_2"></a>This is section 1</h2>
<div class="sectionbody">
<div class="paragraph">
<p>With a paragraph with two sentences, with duplication.
The second sentence.</p>
</div>
<div class="sect2">
<h3 id="${idprefix2}this_is_section_2_2"><a class="anchor" href="#${idprefix2}this_is_section_2_2"></a>This is section 2</h3>
<div class="paragraph">
<p>With a paragraph with two sentences, with duplication.
The second sentence.</p>
</div>
<div class="paragraph">
<p>xref to section 1, this page <a href="#${idprefix2}this_is_section_1_2">This is section 1</a>.
xref to section 2, this page <a href="#${idprefix2}this_is_section_2_2">This is section 2</a>.</p>
</div>
</div>
</div>
</div>
</div>
`)
    })

    it(`included listToAinclude ${name}`, () => {
      const file = contentCatalog.getById({ component: 'component-a', version: '4.5', module: 'module-a', family: 'page', relative: 'doc2.adoc' })
      const idprefix = getIdprefix('_4_5_component_a_module_a_page_page_section_adoc_')
      const idprefix2 = getIdprefix('_4_5_component_a_module_a_page_page_section2_adoc_')
      const doc = loadAsciiDoc(file, contentCatalog, asciidocConfig)
      expect(doc.blocks.length).to.equal(2)
      const html = doc.convert()
      expect(html).to.equal(`<div class="title-document">
  <h1>This is doc 2</h1>
  
</div>
<div style="display: none;"><ul class="sectlevel1">
<li class="toc-entry"><a href="#${idprefix}this_is_section_1">This is section 1</a></li>
<li class="toc-sublist">
<ul class="sectlevel2">
<li class="toc-entry"><a href="#${idprefix}this_is_section_2">This is section 2</a></li>
</ul>
</li>
<li class="toc-entry"><a href="#${idprefix2}this_is_section_1_2">This is section 1</a></li>
<li class="toc-sublist">
<ul class="sectlevel2">
<li class="toc-entry"><a href="#${idprefix2}this_is_section_2_2">This is section 2</a></li>
</ul>
</li>
</ul></div>

<div id="content" class="content">
  <div class="sect1">
<h2 id="${idprefix}this_is_section_1"><a class="anchor" href="#${idprefix}this_is_section_1"></a>This is section 1</h2>
<div class="sectionbody">
<div class="paragraph">
<p>With a paragraph with two sentences.
The second sentence.</p>
</div>
<div class="sect2">
<h3 id="${idprefix}this_is_section_2"><a class="anchor" href="#${idprefix}this_is_section_2"></a>This is section 2</h3>
<div class="paragraph">
<p>With a paragraph with two sentences.
The second sentence.</p>
</div>
<div class="paragraph">
<p>xref to section 1, this page <a href="#${idprefix}this_is_section_1">This is section 1</a>.
xref to section 2, this page <a href="#${idprefix}this_is_section_2">This is section 2</a>.</p>
</div>
</div>
</div>
</div>
<div class="sect1">
<h2 id="${idprefix2}this_is_section_1_2"><a class="anchor" href="#${idprefix2}this_is_section_1_2"></a>This is section 1</h2>
<div class="sectionbody">
<div class="paragraph">
<p>With a paragraph with two sentences, with duplication.
The second sentence.</p>
</div>
<div class="sect2">
<h3 id="${idprefix2}this_is_section_2_2"><a class="anchor" href="#${idprefix2}this_is_section_2_2"></a>This is section 2</h3>
<div class="paragraph">
<p>With a paragraph with two sentences, with duplication.
The second sentence.</p>
</div>
<div class="paragraph">
<p>xref to section 1, this page <a href="#${idprefix2}this_is_section_1_2">This is section 1</a>.
xref to section 2, this page <a href="#${idprefix2}this_is_section_2_2">This is section 2</a>.</p>
</div>
</div>
</div>
</div>
</div>
`)
    })

    it(`cross component/version ainclude ${name}`, () => {
      const file = contentCatalog.getById({ component: 'component-b', version: '4', module: 'module-a', family: 'page', relative: 'parent1.adoc' })
      const idprefix = getIdprefix('_4_5_component_a_module_a_page_page_section_adoc_')
      const doc = loadAsciiDoc(file, contentCatalog, asciidocConfig)
      expect(doc.blocks.length).to.equal(2)
      const html = doc.convert()
      expect(html).to.equal(`<div class="title-document">
  <h1>This is the parent1 document</h1>
  
</div>
<div style="display: none;"><ul class="sectlevel1">
<li class="toc-entry"><a href="#_this_is_a_parent_section">This is a parent section</a></li>
<li class="toc-entry"><a href="#${idprefix}this_is_section_1">This is section 1</a></li>
<li class="toc-sublist">
<ul class="sectlevel2">
<li class="toc-entry"><a href="#${idprefix}this_is_section_2">This is section 2</a></li>
</ul>
</li>
</ul></div>

<div id="content" class="content">
  <div class="sect1">
<h2 id="_this_is_a_parent_section"><a class="anchor" href="#_this_is_a_parent_section"></a>This is a parent section</h2>
<div class="sectionbody">
<div class="paragraph">
<p>The first sentence references the included doc via <a href="#${idprefix}this_is_section_1">This is section 1</a>.
The second sentence references the second section of the included doc via <a href="#${idprefix}this_is_section_2">This is section 2</a>.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="${idprefix}this_is_section_1"><a class="anchor" href="#${idprefix}this_is_section_1"></a>This is section 1</h2>
<div class="sectionbody">
<div class="paragraph">
<p>With a paragraph with two sentences.
The second sentence.</p>
</div>
<div class="sect2">
<h3 id="${idprefix}this_is_section_2"><a class="anchor" href="#${idprefix}this_is_section_2"></a>This is section 2</h3>
<div class="paragraph">
<p>With a paragraph with two sentences.
The second sentence.</p>
</div>
<div class="paragraph">
<p>xref to section 1, this page <a href="#${idprefix}this_is_section_1">This is section 1</a>.
xref to section 2, this page <a href="#${idprefix}this_is_section_2">This is section 2</a>.</p>
</div>
</div>
</div>
</div>
</div>
`)
    })

    ;[
      { relative: 'parent-double-ids.adoc', type: 'plain' },
      { relative: 'parent-double-ids-nav.adoc', type: 'nav' },
    ].forEach(({ relative, type }) => {
      it(`cross component/version ainclude, double ids ${name}, ${type}`, () => {
        const file = contentCatalog.getById({ component: 'component-b', version: '4', module: 'module-a', family: 'page', relative })
        const idprefix1 = getIdprefix('_4_5_component_a_module_a_page_page_section_adoc_')
        const idprefix2 = getIdprefix('_4_5_component_a_module_a_page_page_section2_adoc_')
        const doc = loadAsciiDoc(file, contentCatalog, asciidocConfig)
        expect(doc.blocks.length).to.equal(3)
        const html = doc.convert()
        expect(html).to.equal(`<div class="title-document">
  <h1>This is parent-double-ids.adoc</h1>
  
</div>
<div style="display: none;"><ul class="sectlevel1">
<li class="toc-entry"><a href="#_this_is_a_parent_section">This is a parent section</a></li>
<li class="toc-entry"><a href="#${idprefix1}this_is_section_1">This is section 1</a></li>
<li class="toc-sublist">
<ul class="sectlevel2">
<li class="toc-entry"><a href="#${idprefix1}this_is_section_2">This is section 2</a></li>
</ul>
</li>
<li class="toc-entry"><a href="#${idprefix2}this_is_section_1_2">This is section 1</a></li>
<li class="toc-sublist">
<ul class="sectlevel2">
<li class="toc-entry"><a href="#${idprefix2}this_is_section_2_2">This is section 2</a></li>
</ul>
</li>
</ul></div>

<div id="content" class="content">
  <div class="sect1">
<h2 id="_this_is_a_parent_section"><a class="anchor" href="#_this_is_a_parent_section"></a>This is a parent section</h2>
<div class="sectionbody">
<div class="paragraph">
<p>The first sentence references the included doc via <a href="#${idprefix1}this_is_section_1">This is section 1</a>.
The second sentence references the second section of the included doc via <a href="#${idprefix1}this_is_section_2">This is section 2</a>.</p>
</div>
<div class="paragraph">
<p>The third sentence references the included doc via <a href="#${idprefix2}this_is_section_1_2">This is section 1</a>.
The fourth sentence references the second section of the included doc via <a href="#${idprefix2}this_is_section_2_2">This is section 2</a>.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="${idprefix1}this_is_section_1"><a class="anchor" href="#${idprefix1}this_is_section_1"></a>This is section 1</h2>
<div class="sectionbody">
<div class="paragraph">
<p>With a paragraph with two sentences.
The second sentence.</p>
</div>
<div class="sect2">
<h3 id="${idprefix1}this_is_section_2"><a class="anchor" href="#${idprefix1}this_is_section_2"></a>This is section 2</h3>
<div class="paragraph">
<p>With a paragraph with two sentences.
The second sentence.</p>
</div>
<div class="paragraph">
<p>xref to section 1, this page <a href="#${idprefix1}this_is_section_1">This is section 1</a>.
xref to section 2, this page <a href="#${idprefix1}this_is_section_2">This is section 2</a>.</p>
</div>
</div>
</div>
</div>
<div class="sect1">
<h2 id="${idprefix2}this_is_section_1_2"><a class="anchor" href="#${idprefix2}this_is_section_1_2"></a>This is section 1</h2>
<div class="sectionbody">
<div class="paragraph">
<p>With a paragraph with two sentences, with duplication.
The second sentence.</p>
</div>
<div class="sect2">
<h3 id="${idprefix2}this_is_section_2_2"><a class="anchor" href="#${idprefix2}this_is_section_2_2"></a>This is section 2</h3>
<div class="paragraph">
<p>With a paragraph with two sentences, with duplication.
The second sentence.</p>
</div>
<div class="paragraph">
<p>xref to section 1, this page <a href="#${idprefix2}this_is_section_1_2">This is section 1</a>.
xref to section 2, this page <a href="#${idprefix2}this_is_section_2_2">This is section 2</a>.</p>
</div>
</div>
</div>
</div>
</div>
`)
      })
    })

    it(`topic pages ${name}`, () => {
      const file = contentCatalog.getById({ component: 'component-a', version: '1.0', module: 'module-a', family: 'page', relative: 'simple/parent.adoc' })
      const idprefix1 = getIdprefix('_1_0_component_a_module_a_page_simple_first_child_adoc_')
      const idprefix2 = getIdprefix('_1_0_component_a_module_a_page_simple_second_child_adoc_')
      const doc = loadAsciiDoc(file, contentCatalog, asciidocConfig)
      expect(doc.blocks.length).to.equal(3)
      const html = doc.convert()
      expect(html).to.equal(`<div class="title-document">
  <h1>Ainclude parent page</h1>
  
</div>
<div style="display: none;"><ul class="sectlevel1">
<li class="toc-entry"><a href="#${idprefix1}this_is_a_first_child_page_for_the_simple_ainclude_example">This is a first child page for the simple ainclude example</a></li>
<li class="toc-sublist">
<ul class="sectlevel2">
<li class="toc-entry"><a href="#${idprefix1}this_is_the_second_section_of_the_first_child_page">This is the second section of the first child page</a></li>
</ul>
</li>
<li class="toc-entry"><a href="#${idprefix2}this_is_a_second_child_page_for_the_simple_ainclude_example">This is a second child page for the simple ainclude example</a></li>
<li class="toc-sublist">
<ul class="sectlevel2">
<li class="toc-entry"><a href="#${idprefix2}this_is_the_second_section_of_the_second_child_page">This is the second section of the second child page</a></li>
</ul>
</li>
</ul></div>

<div id="content" class="content">
  <div class="paragraph">
<p>This is a simple example of <code>ainclude</code> functionality</p>
</div>
<div class="sect1">
<h2 id="${idprefix1}this_is_a_first_child_page_for_the_simple_ainclude_example"><a class="anchor" href="#${idprefix1}this_is_a_first_child_page_for_the_simple_ainclude_example"></a>This is a first child page for the simple ainclude example</h2>
<div class="sectionbody">
<div class="paragraph">
<p>Here is some content, linking to the first section of the second page: <a href="#${idprefix2}this_is_a_second_child_page_for_the_simple_ainclude_example">This is a second child page for the simple ainclude example</a>.</p>
</div>
<div class="sect2">
<h3 id="${idprefix1}this_is_the_second_section_of_the_first_child_page"><a class="anchor" href="#${idprefix1}this_is_the_second_section_of_the_first_child_page"></a>This is the second section of the first child page</h3>
<div class="paragraph">
<p>Here is some content, linking to the second section of the second child page: <a href="#${idprefix2}this_is_the_second_section_of_the_second_child_page">This is the second section of the second child page</a>.</p>
</div>
<div class="paragraph">
<p>Here is a link to the custom content on the second page: <a href="#${idprefix2}custom_id_2">Custom Id</a>.</p>
</div>
<div class="paragraph">
<p>Here is a link to the custom content on this page: <a href="#${idprefix1}custom_id">Custom Id</a></p>
</div>
<hr>
<div id="${idprefix1}custom_id" class="paragraph">
<p>Here is a paragraph with a custom Id.</p>
</div>
</div>
</div>
</div>
<div class="sect1">
<h2 id="${idprefix2}this_is_a_second_child_page_for_the_simple_ainclude_example"><a class="anchor" href="#${idprefix2}this_is_a_second_child_page_for_the_simple_ainclude_example"></a>This is a second child page for the simple ainclude example</h2>
<div class="sectionbody">
<div class="paragraph">
<p>Here is some content, linking to the first section of the first page: <a href="#${idprefix1}this_is_a_first_child_page_for_the_simple_ainclude_example">This is a first child page for the simple ainclude example</a>.</p>
</div>
<div class="sect2">
<h3 id="${idprefix2}this_is_the_second_section_of_the_second_child_page"><a class="anchor" href="#${idprefix2}this_is_the_second_section_of_the_second_child_page"></a>This is the second section of the second child page</h3>
<div class="paragraph">
<p>Here is some content, linking to the second section of the first child page: <a href="#${idprefix1}this_is_the_second_section_of_the_first_child_page">This is the second section of the first child page</a>.</p>
</div>
<div class="paragraph">
<p>Here is a link to the custom content on the first page: <a href="#${idprefix1}custom_id">Custom Id</a>.</p>
</div>
<div class="paragraph">
<p>Here is a link to the custom content on this page: <a href="#${idprefix2}custom_id_2">Custom Id</a></p>
</div>
<hr>
<div id="${idprefix2}custom_id_2" class="paragraph">
<p>Here is a paragraph with a custom Id.</p>
</div>
</div>
</div>
</div>
</div>
`)
    })

    ;[
      { relative: 'doc3-list.adoc', style: 'list', getIdprefix },
      { relative: 'doc3-plain.adoc', style: 'plain', getIdprefix },
      { relative: 'doc3-plainest.adoc', style: 'plain include/include', getIdprefix: (prefix) => '_' },
    ].forEach(({ relative, style, getIdprefix }) => {
      it(`${style} ainclude to page with include ${name}`, () => {
        const file = contentCatalog.getById({ component: 'component-a', version: '4.5', module: 'module-a', family: 'page', relative })
        const idprefix = getIdprefix('_4_5_component_a_module_a_page_page_section_including_adoc_')
        const doc = loadAsciiDoc(file, contentCatalog, asciidocConfig)
        expect(doc.blocks.length).to.equal(1)
        const html = doc.convert()
        expect(html).to.equal(`<div class="title-document">
  <h1>This is doc 1</h1>
  
</div>
<div style="display: none;"><ul class="sectlevel1">
<li class="toc-entry"><a href="#${idprefix}this_is_including_section_1">This is including section 1</a></li>
<li class="toc-sublist">
<ul class="sectlevel2">
<li class="toc-entry"><a href="#${idprefix}this_is_included_section_1">This is included section 1</a></li>
</ul>
</li>
</ul></div>

<div id="content" class="content">
  <div class="sect1">
<h2 id="${idprefix}this_is_including_section_1"><a class="anchor" href="#${idprefix}this_is_including_section_1"></a>This is including section 1</h2>
<div class="sectionbody">
<div class="paragraph">
<p>With a paragraph with two sentences.
The second sentence.</p>
</div>
<div class="sect2">
<h3 id="${idprefix}this_is_included_section_1"><a class="anchor" href="#${idprefix}this_is_included_section_1"></a>This is included section 1</h3>
<div class="paragraph">
<p>With a paragraph with two sentences, with duplication.
The second sentence.</p>
</div>
</div>
</div>
</div>
</div>
`)
      })
    })
  })
})
