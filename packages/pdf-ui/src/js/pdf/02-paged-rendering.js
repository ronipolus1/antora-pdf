'use strict'

import { registerHandlers, Handler, Previewer } from './paged.js'

window.AntoraPDF = window.AntoraPDF || {}
window.AntoraPDF.status = 'rendering'
class PagedReadyHandler extends Handler {
  afterRendered (pages) {
    window.AntoraPDF.status = 'ready'
  }

  // BEGIN: workaround to prevent duplicated content at end/beginning of page
  // https://gitlab.pagedmedia.org/tools/pagedjs/issues/126
  constructor (chunker, polisher, caller) {
    super(chunker, polisher, caller)
    this.carriageReturn = String.fromCharCode(10)
  }

  checkNode (node) {
    if (!node) return
    if (node.nodeType !== 3) return
    if (node.textContent === this.carriageReturn) {
      node.remove()
    }
  }

  afterParsed (parsed) {
    const template = document.querySelector('template').content
    const breakAfterAvoidElements = template.querySelectorAll('[data-break-after="avoid"], [data-break-before="avoid"]')
    for (const el of breakAfterAvoidElements) {
      this.checkNode(el.previousSibling)
      this.checkNode(el.nextSibling)
    }
  }
  // END: workaround to prevent duplicated content at end/beginning of page
}

class RepeatingTableHeaders extends Handler {
  // eslint-disable-next-line no-useless-constructor
  constructor (chunker, polisher, caller) {
    super(chunker, polisher, caller)
  }

  afterPageLayout (pageElement, page, breakToken, chunker) {
    // Find all split table elements
    const tables = pageElement.querySelectorAll('table[data-split-from]')

    tables.forEach((table) => {
      // Get the reference UUID of the node
      const ref = table.dataset.ref
      // Find the node in the original source
      const sourceTable = chunker.source.querySelector("[data-ref='" + ref + "']")
      // Find if there is a header
      const header = sourceTable.querySelector('thead')
      if (header) {
        // Clone the header element
        const clonedHeader = header.cloneNode(true)
        // Insert the header at the start of the split table
        table.insertBefore(clonedHeader, table.firstChild)
      }
    })
  }
}

class RenderNotifier extends Handler {
  // eslint-disable-next-line no-useless-constructor
  constructor (chunker, polisher, caller) {
    super(chunker, polisher, caller)
  }

  afterPageLayout (pageElement, page, breakToken, chunker) {
    console.log('Page layout completed ', page.position)
  }
}

registerHandlers(PagedReadyHandler, RepeatingTableHeaders, RenderNotifier)

window.PagedConfig = window.PagedConfig || {}

window.PagedConfig.auto = false

// same logic as pagedjs, but waiting for 'complete' instead of 'interactive'
const ready = new Promise(function (resolve, reject) {
  if (document.readyState === 'complete') {
    resolve(document.readyState)
    return
  }

  document.onreadystatechange = function () {
    if (document.readyState === 'complete') {
      resolve(document.readyState)
    }
  }
})

//why can't this be written using async/await??? eslint objects!
ready.then(function () {
  const done = new Promise(function (resolve, reject) {
    resolve(
      new Previewer(window.PagedConfig).preview(
        window.PagedConfig.content,
        window.PagedConfig.stylesheets,
        window.PagedConfig.renderTo
      )
    )
  }).then(
    new Promise(function (resolve, reject) {
      if (window.PagedConfig.after) {
        resolve(window.PagedConfig.after(done))
      } else {
        resolve(true)
      }
    })
  )
})
